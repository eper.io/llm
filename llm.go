package main

import (
	"bufio"
	"bytes"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

func main() {
	t, _ := os.ReadFile("training.txt")
	tr := string(t)
	fmt.Println("--- The initial training set ---")
	fmt.Println(tr)
	layer1 := momentumTokens(tr)
	fmt.Println("--- The first layer model ---")
	fmt.Println(layer1)
	layer2 := montecarloRules(tr, layer1)
	fmt.Println("--- The final model ---")
	fmt.Println(destanitizeString(layer2))
}

// momentumTokens is the first layer of the logic.
// We use a lazy, depth first algorithm to all tokens them easily.
// A token has at least two full occurrences in a paragraph.
// Tokens do not spread across paragraphs.
// Tokens are concise. We can group them into rules anyway.
func momentumTokens(training string) string {
	tokens := sanitizeString(training)
	layer1 := map[string]int{}
	for i := 0; i < len(tokens); i++ {
		t := tokens[i]
		if i < len(tokens)-2 {
			merged := t + " " + tokens[i+1]
			x := strings.Count(training, t)
			if x >= 3 && x == strings.Count(training, merged) {
				superMerged := merged + " " + tokens[i+2]
				if strings.Count(training, merged) == strings.Count(training, superMerged) {
					t = superMerged
					i++
					i++
				} else {
					t = merged
					i++
				}
			}
		}
		n := layer1[t]
		n++
		layer1[t] = n
	}

	layer1Model := bytes.Buffer{}

	for v, _ := range layer1 {
		rule := v
		if rule != "" {
			_, _ = layer1Model.WriteString(fmt.Sprintln(fmt.Sprintf("This is a layer one rule %s #", rule)))
		}
	}
	return layer1Model.String()
}

func sanitizeString(training string) []string {
	x := strings.ReplaceAll(training, "\r", "")
	x = strings.ReplaceAll(x, "\n", " ")
	x = strings.ReplaceAll(x, "?", " question ")
	x = strings.ReplaceAll(x, ".", " period ")
	x = strings.ReplaceAll(x, "!", " exclamation ")
	tokens := strings.Split(x, " ")
	return tokens
}

func destanitizeString(s string) string {
	x := strings.ReplaceAll(s, " period", ".")
	x = strings.ReplaceAll(x, " question", "?")
	x = strings.ReplaceAll(x, " exclamation", "!")
	return x
}

// montecarloRules finds rules over tokens.
// Rules have at least two samples qualify for the training set.
// Montecarlo is a simple random sampling to find rules.
// Montecarlo allows us to spread across machines horizontally and to identify more layers later.
func montecarloRules(training, model string) string {
	scanner := bufio.NewScanner(strings.NewReader(model))
	rules := map[string]int{}
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "This is a layer one rule ") {
			line = line[len("This is a layer one rule "):]
		}
		if strings.HasSuffix(line, " #") {
			line = line[0 : len(line)-len(" #")]
		}
		if line != "" {
			rules[line] = 1
		}
	}

	scanner = bufio.NewScanner(strings.NewReader(training))
	layer2 := map[string][]string{}
	layer2ForRandom := make([][]string, 0)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.Join(sanitizeString(line), " ")
		rule := make([]string, 0)
		for len(line) > 0 {
			current := len(line)
			for t, _ := range rules {
				if strings.HasPrefix(line, t) {
					rule = append(rule, t)
					line = strings.Trim(line[len(t):], " ")
					break
				}
			}
			if current == len(line) {
				break
			}
		}
		_, ok := layer2[strings.Join(rule, "")]
		if !ok {
			layer2ForRandom = append(layer2ForRandom, rule)
		}
		layer2[strings.Join(rule, "")] = rule
	}

	start := time.Now()
	for time.Now().Sub(start).Seconds() <= 5 && len(layer2ForRandom) >= 2 {
		ts1, ts2 := randomRules(layer2ForRandom)
		if len(ts1) == 0 {
			continue
		}
		if len(ts1) == 0 {
			continue
		}
		if len(ts2) < len(ts1) {
			continue
		}
		if ts1[0] != ts2[0] {
			// Performance hint
			continue
		}
		if len(ts2) > len(ts1) {
			matches := momentumGenerativeLogic(ts1, ts2, start)
			rule := makeRuleFromMatches(matches, ts2, ts1)
			_, ok := layer2[strings.Join(rule, "")]
			if !ok {
				layer2ForRandom = append(layer2ForRandom, rule)
			}
			layer2[strings.Join(rule, "")] = rule
		} else {
			matches := map[int]int{}
			for time.Now().Sub(start).Seconds() <= 5 && len(ts1) >= 2 {
				i, j := randomItems(len(ts1))
				if ts1[i] == ts2[i] {
					matches[i] = -1
				} else if ts1[i] == ts1[j] {
					matches[i] = j
					matches[j] = i
				} else if len(ts1) < len(ts2) {
					matches[i] = -2
				}

				if len(matches) == len(ts1) && len(matches) == len(ts2) {
					// New rule found with 2+ rule matches
					break
				}
			}
			rule := makeRuleFromMatches(matches, ts2, ts1)
			_, ok := layer2[strings.Join(rule, "")]
			if !ok {
				layer2ForRandom = append(layer2ForRandom, rule)
			}
			layer2[strings.Join(rule, "")] = rule
		}
	}

	layer2Model := bytes.Buffer{}
	for _, x := range layer2ForRandom {
		layer2Model.WriteString(strings.Join(x, " "))
		layer2Model.WriteString("\n")
	}
	return layer2Model.String()
}

func makeRuleFromMatches(matches map[int]int, ts2 []string, ts1 []string) []string {
	rule := make([]string, len(matches))
	for i := 0; i < len(matches); i++ {
		j := matches[i]
		if j == -1 {
			rule[i] = ts2[i]
		} else if j >= 0 {
			if matches[j] == -4 {
				rule[i] = ts1[j]
			} else if i < j {
				rule[i] = fmt.Sprintf("{%d}", i)
			} else {
				rule[i] = fmt.Sprintf("{%d}", j)
			}
		} else if j == -4 {
			rule[i] = ts1[i]
		}
	}
	return rule
}

// momentumGenerativeLogic tries to apply and extend the first rule with the second one.
func momentumGenerativeLogic(ts1 []string, ts2 []string, start time.Time) (matches map[int]int) {
	matches = map[int]int{}
	for k := 0; k < len(ts2); k++ {
		matches[k] = -3
	}
	for time.Now().Sub(start).Seconds() <= 5 {
		// Generative logic
		i, j := randomItems(len(ts2))
		if i < len(ts1) && ts1[i] == ts2[i] {
			matches[i] = -1
		} else if i < len(ts1) && ts1[i] != ts2[i] {
			if ts2[i] == ts2[j] {
				matches[i] = -4
				matches[j] = i
			}
		}

		finished := true
		for k := 0; k < len(ts1); k++ {
			if matches[k] == -3 {
				finished = false
			}
		}
		if finished {
			for k := len(ts1); k < len(ts2); k++ {
				if matches[k] == -3 {
					matches[k] = -1
				}
			}
			break
		}
	}
	return
}

// randomRules returns two distinct random rules from a model
func randomRules(model [][]string) (a []string, b []string) {
	i, j := randomItems(len(model))
	a, b = model[i], model[j]
	return
}

// randomItems generates two distinct random numbers in a range.
func randomItems(n int) (i, j int) {
	i = rand.Intn(n)
	j = (i + 1 + rand.Intn(n-1)) % n
	return
}
